// animator of the animation resize, renders the given number of sprites, playing a zoom in or out between from and to percentage values
export const resize = async (animation, img, canvas, sizex, sizey) => {
  return {
    nb: animation.nbsprites,
    draw: (i) => {
      let coef = 1,
        nbsp = animation.nbsprites;
      coef =
        animation.from / 100 +
        ((i / (nbsp - 1)) * (animation.to - animation.from)) / 100;
      let baseresize = 1;
      if (img.width / sizex < img.height / sizey) {
        baseresize = sizey / img.height;
      } else {
        baseresize = sizex / img.width;
      }
      if (canvas) {
        // draw sprite i on canvas
        let ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, sizex, sizey);
        ctx.drawImage(
          img,
          0,
          0,
          img.width,
          img.height,
          sizex / 2 - (img.width * baseresize * coef) / 2,
          sizey / 2 - (img.height * baseresize * coef) / 2,
          img.width * baseresize * coef,
          img.height * baseresize * coef
        );
      }
    },
  };
};

// animator of the animation rotate, renders the given number of sprites, playing a rotation of the image between from and to
export const rotate = async (animation, img, canvas, sizex, sizey) => {
  return {
    nb: animation.nbsprites,
    draw: (i) => {
      let nbsp = animation.nbsprites;
      let angle =
        ((animation.from + (i / (nbsp - 1)) * (animation.to - animation.from)) *
          Math.PI) /
        180;
      let baseresize = 1;
      if (img.width / sizex < img.height / sizey) {
        baseresize = sizey / img.height;
      } else {
        baseresize = sizex / img.width;
      }
      if (canvas) {
        // draw sprite i on canvas
        let ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, sizex, sizey);
        ctx.save();
        ctx.translate(sizex / 2, sizey / 2);
        ctx.rotate(angle);

        ctx.translate(-sizex / 2, -sizey / 2);
        ctx.drawImage(
          img,
          0,
          0,
          img.width,
          img.height,
          sizex / 2 - (img.width * baseresize) / 2,
          sizey / 2 - (img.height * baseresize) / 2,
          img.width * baseresize,
          img.height * baseresize
        );
        ctx.restore();
      }
    },
  };
};

// animator of the animation existing animation, renders each selected sprite of the original animation
export const existing_animation = async (
  animation,
  img,
  canvas,
  sizex,
  sizey
) => {
  return {
    nb: animation.selected?.length || 0,
    draw: (idx) => {
      if (canvas && animation.selected && animation.selected.length > idx) {
        let sx = animation.nbX > 0 ? (img?.width || 0) / animation.nbX : 0,
          sy = animation.nbY > 0 ? (img?.height || 0) / animation.nbY : 0;
        let baseresize = 1;
        if (sx / sizex < sy / sizey) {
          baseresize = sizey / sy;
        } else {
          baseresize = sizex / sx;
        }
        let { i, j } = animation.selected[idx];
        // draw sprite i on canvas
        let ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, sizex, sizey);
        ctx.drawImage(
          img,
          (i - 1) * sx,
          (j - 1) * sy,
          sx,
          sy,
          sizex / 2 - (sx * baseresize) / 2,
          sizey / 2 - (sy * baseresize) / 2,
          sx * baseresize,
          sy * baseresize
        );
      }
    },
  };
};

//animator of the translate animation, renders only selected sprites, render each sprite with an optional nudge in x and y
export const translate = async (animation, img, canvas, sizex, sizey) => {
  return {
    nb: animation.selected?.length || 0,
    draw: (idx) => {
      if (canvas && animation.selected && animation.selected.length > idx) {
        let sx = animation.nbX > 0 ? (img?.width || 0) / animation.nbX : 0,
          sy = animation.nbY > 0 ? (img?.height || 0) / animation.nbY : 0;
        let baseresize = 1;
        if (sx / sizex < sy / sizey) {
          baseresize = sizey / sy;
        } else {
          baseresize = sizex / sx;
        }
        let { i, j } = animation.selected[idx];
        let nudgeX = animation.sprites[i + "-" + j + "-x"] || 0,
          nudgeY = animation.sprites[i + "-" + j + "-y"] || 0;
        // draw sprite i on canvas
        let ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, sizex, sizey);
        ctx.drawImage(
          img,
          (i - 1) * sx,
          (j - 1) * sy,
          sx,
          sy,
          sizex / 2 - (sx * baseresize) / 2 + nudgeX,
          sizey / 2 - (sy * baseresize) / 2 + nudgeY,
          sx * baseresize,
          sy * baseresize
        );
      }
    },
  };
};

// animator of the import image animation, just reders one sprite, the image
export const import_image = async (animation, img, canvas, sizex, sizey) => {
  return {
    nb: 1,
    draw: (idx) => {
      if (canvas) {
        let baseresize = 1;
        if (img.width / sizex < img.height / sizey) {
          baseresize = sizey / img.height;
        } else {
          baseresize = sizex / img.width;
        }
        // draw sprite i on canvas
        let ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, sizex, sizey);
        ctx.drawImage(
          img,
          0,
          0,
          img.width,
          img.height,
          sizex / 2 - (img.width * baseresize) / 2,
          sizey / 2 - (img.height * baseresize) / 2,
          img.width * baseresize,
          img.height * baseresize
        );
      }
    },
  };
};

// loads an image through a promise
export const getImage = async (url) => {
  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, reject) => {
    let img = new Image();
    img.onload = function () {
      resolve(this);
    };
    img.onerror = (e) => {
      console.error(e);
      resolve(undefined);
    };
    img.src = url;
  });
};

//gets an animator for a given animation, the animator calculates the number of rendered sprites and will render each sprite on demand
export const getAnimator = async (animation, canvas, sizex, sizey) => {
  let type = animation.type;
  if (type && anims[type] && anims[type].animator) {
    let imgdef = animation[anims[type].imagefield || "image"];
    if (imgdef) {
      let img = await getImage(imgdef);
      img = await applyTransformations(animation, img, sizex, sizey);
      if (img) {
        let animator = await anims[type].animator(
          animation,
          img,
          canvas,
          sizex,
          sizey
        );
        return animator;
      } else {
        return {
          nb: 0,
          draw: () => {},
        };
      }
    } else {
      return {
        nb: 0,
        draw: () => {},
      };
    }
  }
};

// plays an animation on a canvas
export const animate = async (
  animation,
  canvas,
  sizex,
  sizey,
  duration = 1000
) => {
  let animator = await getAnimator(animation, canvas, sizex, sizey);
  if (animator) {
    let rafid;
    let ret = {
      start: () => {
        let currentpos = 0;
        const loadNextFrame = () => {
          animator.draw(currentpos);
          currentpos = (currentpos + 1) % animator.nb;
        };
        rafid = setInterval(loadNextFrame, duration / animator.nb);
      },
      stop: () => {
        clearInterval(rafid);
      },
    };
    return ret;
  } else {
    return {
      start: () => {},
      stop: () => {},
    };
  }
};

function rgbToHsl(r, g, b) {
  (r /= 255), (g /= 255), (b /= 255);
  var max = Math.max(r, g, b),
    min = Math.min(r, g, b);
  var h,
    s,
    l = (max + min) / 2;

  if (max == min) {
    h = s = 0; // achromatic
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
    }
    h /= 6;
  }

  return [h, s, l];
}
function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;

    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }

  return [r * 255, g * 255, b * 255];
}
function hexToRgb(hex) {
  const normal = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
  if (normal) return normal.slice(1).map((e) => parseInt(e, 16));

  const shorthand = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
  if (shorthand) return shorthand.slice(1).map((e) => 0x11 * parseInt(e, 16));

  return null;
}
const hueshift = (animation, transformation, image, sizex, sizey) => {
  if (transformation.color) {
    let canvas = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, image.width, image.height);
    ctx.drawImage(image, 0, 0, image.width, image.height);

    let hslPicked = transformation.color;
    let color = rgbToHsl(...hexToRgb(transformation.color)),
      colorhuepicked = color[0] * 360;
    let colorRangeStart = colorhuepicked * (1 - transformation.tolerance / 100),
      colorRangeEnd = colorhuepicked * (1 + transformation.tolerance / 100);

    var pixels = ctx.getImageData(0, 0, image.width, image.height),
      red,
      green,
      blue;
    for (var i = 0; i < pixels.data.length; i += 4) {
      red = pixels.data[i + 0];
      green = pixels.data[i + 1];
      blue = pixels.data[i + 2];

      var hsl = rgbToHsl(red, green, blue);
      var hue = hsl[0] * 360;

      if (hue > colorRangeStart && hue < colorRangeEnd) {
        var newRgb = hslToRgb(
          hsl[0] + transformation.hueshift / 360,
          hsl[1],
          hsl[2]
        );
        pixels.data[i + 0] = newRgb[0];
        pixels.data[i + 1] = newRgb[1];
        pixels.data[i + 2] = newRgb[2];
      }
    }

    ctx.putImageData(pixels, 0, 0);

    return getImage(canvas.toDataURL());
  }
  return image;
};

// Applies a padding on image. Adds a transparent border around it of size (baseresize (ratio rendered image / current image size))
// if image is a spritesheet, applies a padding in each sprite of the spritesheet
const padding = (animation, transformation, image, sizex, sizey) => {
  let imagetype = anims[animation.type].imagetype || "image";
  if (imagetype === "image") {
    // calculates baseresize to deduce padding ratio
    let baseresize = 1;
    if (image.width / sizex < image.height / sizey) {
      baseresize = sizey / image.height;
    } else {
      baseresize = sizex / image.width;
    }
    let new_width =
        transformation.left * baseresize +
        image.width +
        transformation.right * baseresize,
      new_height =
        transformation.top * baseresize +
        image.height +
        transformation.bottom * baseresize;
    // create a canvas of this size, draw old image at right position and return corresponding image
    let canvas = document.createElement("canvas");
    canvas.width = new_width;
    canvas.height = new_height;
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, new_width, new_height);
    ctx.drawImage(
      image,
      0,
      0,
      image.width,
      image.height,
      transformation.left,
      transformation.top,
      image.width,
      image.height
    );
    return getImage(canvas.toDataURL());
  } else if (imagetype === "spritesheet") {
    let nbx = anims[animation.type].nbx || "nbX",
      nby = anims[animation.type].nby || "nbY";
    let sx = animation[nbx] > 0 ? (image?.width || 0) / animation[nbx] : 0,
      sy = animation[nby] > 0 ? (image?.height || 0) / animation[nby] : 0;
    let baseresize = 1;
    if (sx / sizex < sy / sizey) {
      baseresize = sizey / sy;
    } else {
      baseresize = sizex / sx;
    }
    let nsx =
        transformation.left * baseresize +
        sx +
        transformation.right * baseresize,
      nsy =
        transformation.top * baseresize +
        sy +
        transformation.bottom * baseresize,
      new_width = nsx * animation[nbx],
      new_height = nsy * animation[nby];
    // create a canvas of this size, draw old sprites one by one at right position and return corresponding image
    let canvas = document.createElement("canvas");
    canvas.width = new_width;
    canvas.height = new_height;
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, new_width, new_height);
    for (let j = 0; j < animation[nby]; j++) {
      for (let i = 0; i < animation[nbx]; i++) {
        ctx.drawImage(
          image,
          i * sx,
          j * sy,
          sx,
          sy,
          i * nsx + transformation.left,
          j * nsy + transformation.top,
          sx,
          sy
        );
      }
    }
    return getImage(canvas.toDataURL());
  }
  return image;
};

// applies a list of transformations to an image
const applyTransformations = async (animation, image, sizex, sizey) => {
  let currentImage = image;
  if (animation.transformations) {
    for (let transformation of animation.transformations) {
      let type = transformation.type;
      if (type && transformations[type]) {
        currentImage = await transformations[type](
          animation,
          transformation,
          currentImage,
          sizex,
          sizey
        );
      }
    }
  }
  return currentImage;
};

//list of existing transformations
const transformations = {
  padding,
  hueshift,
};
//list of existing animations
const anims = {
  resize: {
    animator: resize,
    imagefield: "image",
    imagetype: "image",
  },
  existing_animation: {
    animator: existing_animation,
    imagefield: "image",
    imagetype: "spritesheet",
    nbx: "nbX",
    nby: "nbY",
  },
  rotate: {
    animator: rotate,
    imagefield: "image",
    imagetype: "image",
  },
  translate: {
    animator: translate,
    imagefield: "image",
    imagetype: "spritesheet",
    nbx: "nbX",
    nby: "nbY",
  },
  import_image: {
    animator: import_image,
    imagefield: "image",
    imagetype: "image",
  },
};
